#!/bin/bash

## A script that converts between Hba1c values and estimated average blood sugar levels.
## References:
# Take values from http://www.ngsp.org/ifccngsp.asp

# Hoelzel W, Weykamp C, Jeppsson JO, Miedema K, Barr JR, Goodall I, Hoshino T, John WG, Kobold U, Little R, Mosca A, Mauri P, Paroni R, Susanto F, Takei I, Thienpont L, Umemoto M, Wiedmeyer HM. IFCC Reference System for Measurement of Hemoglobin A1c in human blood and the national standardization schemes in the United States, Japan and Sweden: a method comparison study. Clin.Chem. 2004; 50: 166-174

# Geistanger A, Arends S, Berding C, Hoshino T, Jeppsson J-O, Little R, Siebelder C and Weykamp C,  on behalf of the IFCC Working Group on Standardization of HbA1c: Statistical Methods for Monitoring the Relationship between the IFCC Reference Measurement Procedure for Hemoglobin A1c and the Designated Comparison Methods in the United States, Japan and Sweden. Clin Chem 2008; 54(8): 1379-8

# Report of the ADA/EASD/IDF Working Group of the HbA1c Assay, London, UK, January 2004. Diabetologia 2004;47:R53-4

# Sacks DB for the ADA/EASD/IDF Working Group of the HbA1c Assay. Global harmonization of Hemglobin A1c. Clin Chem 2005;51:681-3

## Nathan DM, Kuenen J, Borg R, Zheng H, Schoenfeld D, Heine RJ. Translating the A1C assay into estimated average glucose values. Diabetes Care 2008; 31(8):1473-8.
# AGmg/dl = 28.7 × A1C − 46.7, R2 = 0.84, P < 0.0001), allowing calculation of an estimated average glucose (eAG) for A1C values.

######################
## TO DO:
#########################################################

set -eu

clear

echo ""
echo ###################################################################################################################################
echo ""
echo -e "WELCOME\n"
echo -e "Converts between different Hba1c values (Mono-S, IFCC, NGSP) and average glucose levels (mm/L, mg/dL) \n"
echo ""
echo -e "READ THIS!!\n"
echo -e "THERE IS NO GUARANTY on correct conversion.\n"
echo -e "The conversions are made based on the following references.\n"
echo -e ""
cat << EOF
References:
- http://www.ngsp.org/ifccngsp.asp\n

- Hoelzel W, Weykamp C, Jeppsson JO, Miedema K, Barr JR, Goodall I, Hoshino T, John WG, Kobold U, Little R, Mosca A, Mauri P, Paroni R, Susanto F, Takei I, Thienpont L, Umemoto M, Wiedmeyer HM. IFCC Reference System for Measurement of Hemoglobin A1c in human blood and the national standardization schemes in the United States, Japan and Sweden: a method comparison study. Clin.Chem. 2004; 50: 166-174

- Geistanger A, Arends S, Berding C, Hoshino T, Jeppsson J-O, Little R, Siebelder C and Weykamp C,  on behalf of the IFCC Working Group on Standardization of HbA1c: Statistical Methods for Monitoring the Relationship between the IFCC Reference Measurement Procedure for Hemoglobin A1c and the Designated Comparison Methods in the United States, Japan and Sweden. Clin Chem 2008; 54(8): 1379-8

- Report of the ADA/EASD/IDF Working Group of the HbA1c Assay, London, UK, January 2004. Diabetologia 2004;47:R53-4

- Sacks DB for the ADA/EASD/IDF Working Group of the HbA1c Assay. Global harmonization of Hemglobin A1c. Clin Chem 2005;51:681-3

- Nathan DM, Kuenen J, Borg R, Zheng H, Schoenfeld D, Heine RJ. Translating the A1C assay into estimated average glucose values. Diabetes Care 2008; 31(8):1473-8.
 	AGmg/dl = 28.7 × A1C − 46.7, R2 = 0.84, P < 0.0001), allowing calculation of an estimated average glucose (eAG) for A1C values.
EOF

echo ##################################################################################################################################
echo ""
echo "What do you want to convert FROM? Select a unit or the corresponding number."
echo ""
read -p "Mono-S (1); NGSP (2); IFCC (3); mm/L (4); mg/dL (5): " FVal
echo ""

## Make some functions for the conversions

FromMnS() {
	ifcc=$( echo "scale=3;  ((10.11 * $1) - 8.94)" | bc )
	NG=$( echo "scale=3;  ((0.09148 * ((10.11 * $1)-8.94)) + 2.152)" | bc )
	mml=$( echo "scale=3; ((28.7 * ((0.09148 * ((10.11 * $1) - 8.94)) + 2.152)) - 46.7)/18" | bc )
	mgdl=$( echo "scale=3; ((28.7 * ((0.09148 * ((10.11 * $1) - 8.94)) + 2.152)) - 46.7)" | bc )
	
	echo "Which corresponds to:"
	echo "IFCC: ${ifcc} %"
	echo "NGSP: ${NG} %"
	echo "Average blood sugar in mm/L: ${mml}"
	echo "Average blood sugar in  mg/dL: ${mgdl}" 

}


FromNGSP() {
	MnS=$( echo "scale=3;  ((0.09890 * ((10.93*$NG) - 23.50)) +0.884)" | bc )
	ifcc=$( echo "scale=3; (10.93 * $NG) - 23.50" | bc )
	mml=$( echo "scale=3;  ((28.7 * $NG) - 46.7)/18" | bc )
	mgdl=$( echo "scale=3;  (28.7 * $NG) - 46.7" | bc )
	
	echo "Which corresponds to:"
	echo "Mono-S: ${MnS} %"
	echo "IFCC: ${ifcc} %"
	echo "Average blood sugar in mm/L: ${mml}"
	echo "Average blood sugar in mg/dL: ${mgdl}"
}

FromIFCC() {
	MnS=$( echo "scale=3;  ((0.09890 * $1) + 0.884)" | bc )
	NG=$( echo "scale=3;  ((0.09148 * $1) + 2.152)" | bc )
	mml=$( echo "scale=3; ((28.7 * ((0.09148 * $1) + 2.152)) - 46.7)/18" | bc )
	mgdl=$( echo "scale=3; ((28.7 * ((0.09148 * $1) + 2.152)) - 46.7)" | bc )

	echo "Which corresponds to:"
	echo "Mono-S: ${MnS} %"
	echo "NGSP: ${NG} %"
	echo "Average blood sugar in mm/L: ${mml}"
	echo "Average blood sugar in mg/dL: ${mgdl}"

}

FromMmmol() {
	MnS=$( echo "scale=3; ((0.09890 * ((10.93*(($1 * 18 + 46.7) / 28.7)) - 23.50)) +0.884) " | bc )
	NG=$( echo "scale=3; ($1 * 18 + 46.7) / 28.7 " | bc )
	ifcc=$( echo "scale=3;  (((($1 * 18) + 46.7) / 28.7) * 10.93) - 23.50" | bc )
	mgdl=$( echo "scale=3; (18 * $1)" | bc )

	echo "Which corresponds to:"
	echo "Mono-S: ${MnS} %"
	echo "NGSP: ${NG} %"
	echo "IFCC: ${ifcc} %"
	echo "Average blood sugar in mg/dL: ${mgdl}."

}

FromMgdl() {
	MnS=$( echo "scale=3; ((0.09890 * ((10.93*(($1 + 46.7) / 28.7)) - 23.50)) +0.884)"  | bc )
	NG=$( echo "scale=3;  ($1 + 46.7) / 28.7"  | bc )
	ifcc=$( echo "scale=3;  ((($1 + 46.7) / 28.7) * 10.93) - 23.50" | bc )
	mml=$( echo "scale=3; ($1/18)" | bc)

	echo "Which corresponds to:"
	echo "Mono-S: ${MnS} %"
	echo "NGSP: ${NG} %"
	echo "IFCC: ${ifcc} %"
	echo "Average blood sugar in mm/L: ${mml}"

}


case $FVal in
# From Mono-S Sweden
	Mono-S|1) ## FINISHED

		clear
		read -p "What is your Mono-S value %: " MnS
		echo ""
		FromMnS "$MnS" ;;
		
# From NGSP
	NGSP|2) # FINISHED
		clear
		read -p "What is your NGSP value %: " NG
		echo ""
		FromNGSP "$NG" ;;

# From IFCC
	IFCC|3) ## FINISHED
		clear
		read -p "What is your IFCC value %: " IFCC
		echo ""
		FromIFCC "$IFCC";;

## From mm/L
	mm/L|4) ## FINISHED

		clear
		read -p "What is your mm/L value: " mml
		echo ""
		FromMmmol "$mml" ;;

## From mg/dL
	mg/dL|5) ## FINISHED

		clear
		read -p "What is your mg/dL value: " mgdl
		echo ""
		FromMgdl "$mgdl" ;;
esac
