# Diabetestools

Some scripts, in Bash and R, to help calculate use of diabetes medicins and how long they last.

--------------------------------------
### BASH script

Copy file to, for example, the ` $HOME/bin` directory. Then make the file executable, e.g.:

`chmod +x $HOME/bin/InsulinUse.sh`

If `$HOME/bin` is in your `$PATH` variable you, just run `InsulinUse.sh` and follow instructions.


#### InsulinUse.sh

The Bash script will ask you a number of questions based on what you want to calculate.

- How much you need to bring for a ceartain number of days
- How many days the current stock will last.

The days left before the Insulin will be used up can be based on number of units or number of ampulls left. If you provide number of units left that will be used for the calculations.
In difference to the R-script InsulinUsage.R, the bash script will give you the date the stuff will be used up.


#### InsuCalc_gui.sh

This is the GUI version of the InsulinUse.sh script based on zenity.


#### Hba1c.sh

This script can be used to convert between a number of different Hba1c used in different countries (NGSP, Mono-S and IFCC) and also avarage blood sugar in mg/dL and mm/L.

----------------------------------
### R script

#### InsulinUsage.R

The R script contains two functions.:
-  `Insulin` calculates how much you need to bring for a certain amount of days.

- `InsulinDur` calculates how many days the current stock will last.

Source the script

`source("$HOME/InsulinUsage.R")`

The **first function** is `Insulin()`
You need to specify:

- DayInUse = *The amount of fast insulin you use in units per day, __default to 24__*.
- NightInUse = *The amount night insulin you use in units per day, __default to 16__*.
- NrDays = *How many days you are going to be away.*
- NeedlesUse = *The number of needles you use per day, __default to 2__*.
- BlTestUse = *How many blood sugar test sticks you use per day, __default to 4__*.
- ToFile = FALSE *or* TRUE (F/T) *If you wanty to save the info to a ; separated csv file, defaults to* F.


The **second function** is `InsulinDur `
You need to specify in:

- DayIn = *The amount of Insulin you have left.*
		- DayIAmp = *The number of Day insulin ampulls left.*
- NightIn = *The amount of Night insulin you have left.*
		- NightIAmp = *The number of nightinsulin ampulls left.*
- Needl = *The amouunt of Needles you have left.*
- BloodTest = *The amount of glucos test sticks you have left.*
- ToFile = FALSE *or* TRUE (F/T) *If you wanty to save the info to a ; separated csv file, defaults to F.*

The amount used per day defaults to the same as in the above function `Insulin` but you can change them to by stating the same variables as above with the correct value.
