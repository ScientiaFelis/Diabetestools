#!/bin/bash
set -eu

clear
echo ############################################################################################
echo ""
echo " >> INSULINUSE << "
echo ""
echo ##############################################################################################

echo #######################################################
echo "Calculates the insulin usage over a time period."
echo ""
echo "OR"
echo ""
echo "The number of days left on a given amount of equipment."
#####################################################################
echo ""
echo "Do you want to calculate the consumption in a nr of days - 1"
echo "Or how many days you equipment will last - 2"
read -p "1 or 2: " Calc

if [[ $Calc -ne 1 && $Calc -ne 2 ]]; then
	echo "You need to put in 1 or 2."
	read -p "Do you want to calculate how much you'll use in a nr of days (1) or how many days you equipment will last (2)" Calc
fi

clear

        ## Setting date
		datum=$(date +"%Y-%m-%d")
		## Setting package sizes.
		DayAmpSize=300 ## Size of day insulin ampulls
		NightAmpSize=300 ## Size of night insulin ampulls
		DayPackageAmpNr=5 ## Nr of ampulls ina packege for day insulin
		NightPackageAmpNr=5 ## Nr of ampulls ina packege for night insulin
		NeedlesPack=100 ## Nr of needles in a package
		TestPack=50 ## Nr of test sticks in a package


if [[ $Calc -eq 1 ]]; then

## HERE WE CALCULATE HOW MUCH EQUIPMENT YOU WILL NEED IN A CERTAIN NR OF DAYS.

	read -p "Do you have two dates (1) or a number of days (2)?: " Dform
	if [[ $Dform -eq 1 ]]; then

		read -p "Start date in Y-m-d format: " start_ts
		read -p "End date in Y-m-d format: " end_ts
		
		end_ts=$(date -d "$end_ts" '+%s')
		start_ts=$(date -d "$start_ts" '+%s')
		
		NrDays=$(printf "%.0f" $(echo "scale=2; ( $end_ts - $start_ts )/(60*60*24)" | bc | sed 's/\./,/'))

	else

		read -p "Number of days: " NrDays
	fi

		echo ""
		echo "Default values in parentesis."
		echo ""
		read -p "Daily insulin use in units of rapid day insulin (22): " DayInUse
		read -p "Daily insulin use in units of night insulin (12): " NightInUse
		read -p "Number of needles per day (2): " NeedlesUse
		read -p "Number of glucos teststicks per day (3): " BlTestUse
		read -p "Do you want to store in file (y/N)?: " ToFile

				if [[ $ToFile == "y" ]]; then
						read -p "Do you want to print the results (y/N)? " Pri
				fi

		## Setting some default values
		DayInUse=${DayInUse:=22}
		NightInUse=${NightInUse:=12}
		NeedlesUse=${NeedlesUse:=2}
		BlTestUse=${BlTestUse:=3}


		clear

		  # Day insulin
		Us=$(( $DayInUse * $NrDays )) ## calculate the nr of units of insulin needed
		Amp=$(( $Us / $DayAmpSize )) ## calculate nr of ampulls needed
				if [[ $(( $Us % $DayAmpSize )) -gt 0 ]]; then ## If there is a rest in the above division we have to add another ampull to get enough.
						Amp=$(( $Amp+1 ))
				fi

		Pack=$(( $Amp / $DayPackageAmpNr )) ## Calculate how many packages with ampulls we need.
				if [[ $(( $Amp % $DayPackageAmpNr )) -gt 0 ]]; then ## If there is a rest in the above division we have to ad another package to get enough.
		        Pack=$(( $Pack+1 ))
		    fi

		## Save the numbers in an explaining string
		 DayUse=$(echo "You will use" ${Us} "units of Day insulin during your stay.")
		 Dayneed=$(echo "YOU NEED:" ${Amp} "ampulls of" ${DayAmpSize} "units, or" ${Pack} "PACKAGES each with" ${DayPackageAmpNr} "day insulin ampuls.")


		  ## Make the same calculations for Night Insulin
		NUs=$(( $NightInUse * $NrDays ))
		NAmp=$(( $NUs / $NightAmpSize ))
				if [[ $(( $NUs % $NightAmpSize )) -gt 0 ]]; then
						NAmp=$(( $NAmp+1 ))
				fi

		NPack=$(( $NAmp / $NightPackageAmpNr ))
				if [[ $(( $NAmp % $NightPackageAmpNr )) -gt 0 ]]; then
						NPack=$(( $NPack+1 ))
				fi

		NightUse=$(echo "You will use" ${NUs} "units of Night insulin during your stay.")
		Nightneed=$(echo "YOU NEED:" ${NAmp} "ampulls of" ${NightAmpSize} "units, or" ${NPack} "PACKAGES, each with" ${NightPackageAmpNr} "night insulin ampulls.")


		## And Usage of Needles and Blood tests
		  needls=$(( $NeedlesUse * $NrDays ))
		  needl=$(( ( $NeedlesUse * $NrDays ) / $NeedlesPack ))
		  if [[ $(( ( $NeedlesUse * $NrDays ) % $NeedlesPack )) -gt 0 ]]; then
		       needl=$(( $needl+1 ))
		  fi

	        testst=$(( ( $BlTestUse * $NrDays ) ))
            tests=$(( ( $BlTestUse * $NrDays ) / $TestPack ))
		   if [[ $(( ( $BlTestUse * $NrDays ) % $TestPack )) -gt 0 ]]; then
		       tests=$(( $tests+1 ))
		   fi
		  needles=$(echo "YOU NEED:" ${needls} "needles, or" ${needl} "PACKAGES of needles during your stay.")
		  teststickor=$(echo "YOU NEED:" ${testst} blood glycos teststicks, or ${tests} "PACKAGES of teststicks on your stay.")

		## Write the results

		if [[ $ToFile == "y" ]]; then ## If we chose to save it to a file print the result and save everything in a file.

            echo -e "TODAY IS: ${datum}"

		    echo -e "DAY INSULIN \n"  "${DayUse} \n" "${Dayneed} \n"  "" "NIGHT INSULIN \n" "${NightUse} \n" "${Nightneed} \n"  ""  "NEEDLES AND BLOOD GLYCOSE TESTS \n" "${needles} \n" "${teststickor} \n" > "$HOME/Use of diabetes consumables.txt" ## save everything in one file.

			echo ""
		    echo "The results are saved in the file, Use of diabetes consumables.txt, in your HOME directory"
				if [[ $Pri == "y" ]]; then ## if we also chose to print the file
					lp "$HOME/Use of diabetes consumables.txt"  ## Print it in the default printer.
					echo "The file have also been printed on your default printer."
				fi

		fi
			echo ""
            echo -e "TODAY IS: ${datum}"
			echo ""
		    echo -e "IN YOUR STAY OF ${NrDays} DAYS YOU WILL NEED:\n"
			echo ""
		    echo -e "DAY INSULIN \n"
		    echo -e "${DayUse} \n"
		    echo -e "${Dayneed} \n"
		    echo ""
		    echo -e "NIGHT INSULIN \n"
		    echo -e "${NightUse} \n"
		    echo -e "${Nightneed} \n"
		    echo ""
		    echo -e "NEEDLES AND BLOOD GLYCOSE TESTS \n"
		    echo -e "${needles} \n"
		    echo -e "${teststickor} \n"


else
	## HERE WE CALCULATE THE NUMBER OF DAYS A CERTAIN AMOUNTS OF EQUIPMENT LAST.

		echo "You can chose to set Day and Night insulin in units or nr of ampulls."
		echo "If the latter, just press enter on the question on insulin units."
		echo ""
		echo "Default values in parentesis."
		echo ""
		echo ""
		read -p "Number of Day insulin units left (0): " NrDayIN
		if [[ -z $NrDayIN ]]; then
		read -p "Number of Day insulin ampulls left: " NrAmpL
		fi
		read -p "Number of Night insulin units left (0): " NrNightIN
		if [[ -z $NrNightIN ]]; then
		read -p "Number of Night insulin ampulls left: " NrNAmpL
		fi
		read -p "Number of Teststicks left (0): " NrTestStick
		read -p "Number of Needles left (0): " NrNeedles
		read -p "Daily insulin use in units of rapid insulin (22): " DayInUse
		read -p "Daily insulin use in units of night insulin (12): " NightInUse
		read -p "Number of needles per day (2): " NeedlesUse
		read -p "Number of glucos teststicks per day (3): " BlTestUse
		echo ""
		read -p "Do you want to store in file (y/N)?: " ToFile

				if [[ $ToFile == "y" ]]; then
						read -p "Do you want to print the results (y/N)? " Pri
				fi

		## Setting some default values
		DayInUse=${DayInUse:=22}
		NightInUse=${NightInUse:=12}
		NeedlesUse=${NeedlesUse:=2}
		BlTestUse=${BlTestUse:=3}
		NrDayIN=${NrDayIN:=0} ## we set these to 0 as that will be 0 days.
		NrNightIN=${NrNightIN:=0}
		NrAmpL=${NrAmpL:=0}
		NrNAmpL=${NrNAmpL:=0}
		NrTestStick=${NrTestStick:=0}
		NrNeedles=${NrNeedles:=0}

			clear

		# Day insulin
		if [[ $NrDayIN -eq 0 ]]; then  ## If number of units have been given calculate on that otherwise on ampulls
				NrDays=$(( ($NrAmpL * $DayAmpSize) / $DayInUse ))
		else
				NrDays=$(( $NrDayIN / $DayInUse  )) ## calculate the nr of units of insulin needed
		fi
		
		## Save the numbers in an explaining string
			Dayleft=$(echo You have Day-Insulin for ${NrDays} Days, until $(date --date="+${NrDays} days" +"%a %d %b %Y").)

		## Make the same calculations for Night Insulin
		if [[ $NrNightIN -eq 0 ]]; then
				NrNi=$(( ($NrNAmpL * $NightAmpSize) / $NightInUse ))
		else
		    NrNi=$(( $NrNightIN / $NightInUse ))
		fi

		    NrNight=$(echo You have Night-Insulin for ${NrNi} Days, until $(date --date="+${NrNi} days" +"%a %d %b %Y").)


		## And Usage of Needles and Blood tests

		    NrneDay=$((  $NrNeedles / $NeedlesUse ))
            NrtesDay=$((  $NrTestStick / $BlTestUse ))
            needledays=$(echo You have Needles for ${NrneDay} Days, until $(date --date="+${NrneDay} days" +"%a %d %b %Y").)
            teststickor=$(echo You have Test-sticks for ${NrtesDay} Days, until $(date --date="+${NrtesDay} days" +"%a %d %b %Y").)

		## Write the results

		if [[ $ToFile == "y" ]]; then ## If we chose to save it to a file print the result and save everything in a file.


                echo -e "TODAY IS: ${datum}"
				echo -e "INSULIN \n"  "${Dayleft} \n" "${NrNight} \n"  ""  "NEEDLES AND BLOOD GLYCOSE TESTS \n" "${needledays} \n" "${teststickor} \n" > "$HOME/Nr-of-days-left-of-diabetes-consumables_${datum}.txt" ## save everything in one file.

				echo ""
				echo "The results are saved in the file, Nr of days left of diabetes consumables.txt, in your HOME directory"
				if [[ $Pri == "y" ]]; then ## if we also chose to print the file
						lp "$HOME/Nr-of-days-left-ofdiabetes-consumables.txt"  ## Print it in the default printer.
						echo "The file have also been printed omn your default printer."
				fi

		fi
				 echo ""
                 echo -e "TODAY IS: ${datum}"
				 echo ""
				 echo -e "INSULIN \n"
				 echo -e "${Dayleft} \n"
				 echo -e "${NrNight} \n"
				 echo ""
				 echo -e "NEEDLES AND BLOOD GLYCOSE TESTS \n"
				 echo -e "${needledays} \n"
				 echo -e "${teststickor} \n"

fi
set +eu
