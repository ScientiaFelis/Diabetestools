#!/bin/bash

###############################
## TO DO:
# 
#
######################################


set -eu
zenity --title '>> INSUCALC <<' --no-wrap --info --text="Calculates the insulin usage over a time period,\n OR \n The number of days left on a given amount of equipment."


		## Setting package sizes.
		DayAmpSize=300 ## Size of day insulin ampulls
		NightAmpSize=300 ## Size of night insulin ampulls
		DayPackageAmpNr=5 ## Nr of ampulls ina packege for day insulin
		NightPackageAmpNr=5 ## Nr of ampulls ina packege for night insulin
		NeedlesPack=100 ## Nr of needles in a package
		TestPack=50 ## Nr of test sticks in a package


if zenity --question --no-wrap --text="Do you want to calculate the consumption during a period \n OR \n How many days you equipment will last?" --ok-label="Consumption" --cancel-label="Days equipment last"
then

## HERE WE CALCULATE HOW MUCH EQUIPMENT YOU WILL NEED IN A CERTAIN NR OF DAYS.

	if zenity --question --no-wrap --text="Do you have two dates or a number of days?" --ok-label="Dates" --cancel-label="Nr of Days"
	then
	
		start_ts=$(zenity --calendar --title="Start date" --text="Pick a start date for the period")
		end_ts=$(zenity --calendar --title="End date" --text="Pick a end date for the period")
			
			end_ts=$(date -d "$end_ts" '+%s')
			start_ts=$(date -d "$start_ts" '+%s')
			
			NrDays=$(printf "%.0f" $(echo "scale=2; ( $end_ts - $start_ts )/(60*60*24)" | bc | sed 's/\./,/'))
	
	else
	
		NrDays=$(zenity --entry --text="Number of days of period: " )
	fi

	DayInUse=$(zenity --entry --text="Daily insulin use in units of rapid day insulin (22): ") 
	NightInUse=$(zenity --entry --text="Daily insulin use in units of night insulin (12): ")
	NeedlesUse=$(zenity --entry --text="Number of needles per day (2): ")
	BlTestUse=$(zenity --entry --text="Number of glucos teststicks per day (3): ")

	if zenity --question --no-wrap --title="Store in file" --text="Do you want to store the results in a file?" --ok-label="No" --cancel-label="Yes"
	then
		ToFile="n"
	else

		ToFile="y"
		Pri=$(zenity --question --text="Do you want to print the results?" --ok-label="No" --cancel-label="Yes")
	fi

		## Setting some default values
		DayInUse=${DayInUse:=22}
		NightInUse=${NightInUse:=12}
		NeedlesUse=${NeedlesUse:=2}
		BlTestUse=${BlTestUse:=3}



		  # Day insulin
		Us=$(( $DayInUse * $NrDays )) ## calculate the nr of units of insulin needed
		Amp=$(( $Us / $DayAmpSize )) ## calculate nr of ampulls needed
				if [[ $(( $Us % $DayAmpSize )) -gt 0 ]]; then ## If there is a rest in the above division we have to add another ampull to get enough.
						Amp=$(( $Amp+1 ))
				fi

		Pack=$(( $Amp / $DayPackageAmpNr )) ## Calculate how many packages with ampulls we need.
				if [[ $(( $Amp % $DayPackageAmpNr )) -gt 0 ]]; then ## If there is a rest in the above division we have to ad another package to get enough.
		        Pack=$(( $Pack+1 ))
		    fi

		## Save the numbers in an explaining string
		 DayUse=$(echo "You will use" ${Us} "units of Day insulin during your stay.")
		 Dayneed=$(echo "You need:" ${Amp} "ampulls of" ${DayAmpSize} "units, or" ${Pack} "PACKAGES each with" ${DayPackageAmpNr} "day insulin ampuls.")


		  ## Make the same calculations for Night Insulin
		NUs=$(( $NightInUse * $NrDays ))
		NAmp=$(( $NUs / $NightAmpSize ))
				if [[ $(( $NUs % $NightAmpSize )) -gt 0 ]]; then
						NAmp=$(( $NAmp+1 ))
				fi

		NPack=$(( $NAmp / $NightPackageAmpNr ))
				if [[ $(( $NAmp % $NightPackageAmpNr )) -gt 0 ]]; then
						NPack=$(( $NPack+1 ))
				fi

		NightUse=$(echo "You will use" ${NUs} "units of Night insulin during your stay.")
		Nightneed=$(echo "You need:" ${NAmp} "ampulls of" ${NightAmpSize} "units, or" ${NPack} "PACKAGES, each with" ${NightPackageAmpNr} "night insulin ampulls.")


		## And Usage of Needles and Blood tests
		  needls=$(( $NeedlesUse * $NrDays ))
		  needl=$(( ( $NeedlesUse * $NrDays ) / $NeedlesPack ))
		  if [[ $(( ( $NeedlesUse * $NrDays ) % $NeedlesPack )) -gt 0 ]]; then
		       needl=$(( $needl+1 ))
		  fi

	        testst=$(( ( $BlTestUse * $NrDays ) ))
            tests=$(( ( $BlTestUse * $NrDays ) / $TestPack ))
		   if [[ $(( ( $BlTestUse * $NrDays ) % $TestPack )) -gt 0 ]]; then
		       tests=$(( $tests+1 ))
		   fi
		  needles=$(echo "You will need:" ${needls} "needles, or" ${needl} "PACKAGES of needles during your stay.")
		  teststickor=$(echo "You will need:" ${testst} blood glycos teststicks, or ${tests} "PACKAGES of teststicks on your stay.")


	## HERE WE WRITE OUT THE RESULTS

		if [[ $ToFile == "y" ]]; then ## If we chose to save it to a file print the result and save everything in a file.

			FileS=$(zenity --file-selection --title="Chose a directory" --save --filename="$HOME/" --text "Chose a directory where you want to save the file." --confirm-overwrite)

		    echo -e "DAY INSULIN \n"  "${DayUse} \n" "${Dayneed} \n"  "" "NIGHT INSULIN \n" "${NightUse} \n" "${Nightneed} \n"  ""  "NEEDLES AND BLOOD GLYCOSE TESTS \n" "${needles} \n" "${teststickor} \n" > "$FileS" ## save everything in one file.

#		    echo "The results are saved in the file, Use of diabetes consumables.txt, in your HOME directory"
			if [[ $Pri == "Yes" ]]; then ## if we also chose to print the file
				lp "$FileS"  ## Print it in the default printer.
#k				echo "The file have also been printed on your default printer."
			fi

		fi
	    zenity --info --no-wrap  --window-icon="info" --title="IN YOUR STAY OF ${NrDays} DAYS YOU WILL NEED:" --text="DAY INSULIN \n ${DayUse} \n ${Dayneed} \n \n NIGHT INSULIN \n ${NightUse} \n ${Nightneed} \n \n NEEDLES AND BLOOD GLYCOSE TESTS \n ${needles} \n ${teststickor} \n"


else
	
##### HERE WE CALCULATE THE NUMBER OF DAYS A CERTAIN AMOUNTS OF EQUIPMENT LAST.

		WAU=$(zenity --list --title="Units or Ampulls?" --text="You can chose to set Day and Night insulin in Units or nr of Ampulls." --radiolist --column "Pick" --column "Ampulls or Units" FALSE "Units" TRUE "Ampulls")

		case $WAU in
			*"Units"*) NrDayIN=$(zenity --entry --title="Day Insulin" --text "Number of Day insulin units: ") ;;
			*"Ampulls"*) NrAmpL=$(zenity --entry --title="Day insulin" --text "Number of day insulin ampulls: ") ;;
		esac
		case $WAU in
			*"Units"*) NrNightIN=$(zenity --entry --title="Night Insulin" --text "Number of Night insulin units: ") ;;
			*"Ampulls"*) NrNAmpL=$(zenity --entry --title="Night insulin" --text "Number of Night insulin ampulls: ") ;;
		esac

		NrTestStick=$(zenity --entry --title="Test sticks" --text="Number of Teststicks you have")
		NrNeedles=$(zenity --entry --title="Needles" --text="Number of Needles you have")
		DayInUse=$(zenity --entry --title="Day insulin use" --text="Daily insulin use in units of rapid insulin (22): ")
		NightInUse=$(zenity --entry --title="Night insulin use" --text="Daily insulin use in units of long term insulin (12): ")
		NeedlesUse=$(zenity --entry --title="Needle use" --text="Needles used per day (2)")
		BlTestUse=$( zenity --entry --title="Teststick use" --text="Number of glucos teststicks per day (3): ")

		if zenity --question --no-wrap --title="Store in file" --text="Do you want to store in file?" --ok-label="No" --cancel-label="Yes"
		then
			ToFile="n"
		else
			ToFile="y"

			Pri=$(zenity --question --no-wrap --title="Print results" --text="Do you want to print results" --ok-label="No" --cancel-label="Yes")
		fi

		## Setting some default values
		DayInUse=${DayInUse:=22}
		NightInUse=${NightInUse:=12}
		NeedlesUse=${NeedlesUse:=2}
		BlTestUse=${BlTestUse:=3}
		NrDayIN=${NrDayIN:=0} ## we set these to 0 as that will be 0 days.
		NrNightIN=${NrNightIN:=0}
		NrAmpL=${NrAmpL:=0}
		NrNAmpL=${NrNAmpL:=0}
		NrTestStick=${NrTestStick:=0}
		NrNeedles=${NrNeedles:=0}


		# Day insulin
		if [[ $NrDayIN -eq 0 ]]; then  ## If number of units have been given calculate on that otherwise on ampulls
				NrDays=$(( ($NrAmpL * $DayAmpSize) / $DayInUse ))
		else
				NrDays=$(( $NrDayIN / $DayInUse  )) ## calculate the nr of units of insulin needed
		fi
		
		## Save the numbers in an explaining string
			Dayleft=$(echo You have Day-Insulin for ${NrDays} Days, until $(date --date="+${NrDays} days" +"%a %d %b %Y").)

		## Make the same calculations for Night Insulin
		if [[ $NrNightIN -eq 0 ]]; then
				NrNi=$(( ($NrNAmpL * $NightAmpSize) / $NightInUse ))
		else
		    NrNi=$(( $NrNightIN / $NightInUse ))
		fi

		    NrNight=$(echo You have Night-Insulin for ${NrNi} Days, until $(date --date="+${NrNi} days" +"%a %d %b %Y").)


		## And Usage of Needles and Blood tests

		    NrneDay=$((  $NrNeedles / $NeedlesUse ))
            NrtesDay=$((  $NrTestStick / $BlTestUse ))
            needledays=$(echo You have Needles for ${NrneDay} Days, until $(date --date="+${NrneDay} days" +"%a %d %b %Y").)
            teststickor=$(echo You have Test-sticks for ${NrtesDay} Days, until $(date --date="+${NrtesDay} days" +"%a %d %b %Y").)

		## Write the results

		if [[ $ToFile == "y" ]]; then ## If we chose to save it to a file print the result and save everything in a file.

			FileS=$(zenity --file-selection --title="Chose a directory" --save --filename="$HOME/" --text "Chose a directory where you want to save the file." --confirm-overwrite)


			echo -e "INSULIN \n"  "${Dayleft} \n" "${NrNight} \n"  ""  "NEEDLES AND BLOOD GLYCOSE TESTS \n" "${needledays} \n" "${teststickor} \n" > "$FileS" ## save everything in one file.

			## echo "The results are saved in the file, Nr of days left of diabetes consumables.txt, in your HOME directory"
			if [[ $Pri == "Yes" ]]; then ## if we also chose to print the file
				lp "$FileS"  ## Print it in the default printer.
			##	echo "The file have also been printed omn your default printer."
			fi

		fi
				zenity --info --no-wrap --title="Number of days left" --text="INSULIN \n ${Dayleft} \n ${NrNight} \n \n NEEDLES AND BLOOD GLYCOSE TESTS \n ${needledays} \n ${teststickor} \n"

fi
set +eu



